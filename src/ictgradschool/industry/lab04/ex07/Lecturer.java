package ictgradschool.industry.lab04.ex07;

public class Lecturer {

    // instance variables
    private String name;
    private int staffId;
    private String[] papers;
    private boolean onLeave;
    
    public Lecturer(String name, int staffId, String[] papers, boolean onLeave) {
        // TODO Complete this constructor

        this.name = name;
        this.staffId = staffId;
        this.papers = papers;
        this.onLeave = onLeave;

    }
    
    // TODO Insert getName() method here

    public String getName() {
        return this.name;
    }
    
    // TODO Insert setName() method here

    public void setName(String newName) {
        this.name = newName;
    }
    
    // TODO Insert getStaffId() method here
    public int getStaffId() {
        return this.staffId;
    }
    
    // TODO Insert setStaffId() method here

    public void setStaffId(int newId){
        this.staffId = newId;
    }
    
    // TODO Insert getPapers() method here

    public String[] getPapers() {
        return this.papers;
    }
    
    // TODO Insert setPapers() method here

    public void setPapers(String[] newPaper) {
        this.papers = newPaper;
    }
    
    // TODO Insert isOnLeave() method here

    public boolean isOnLeave() {
        return this.onLeave;
    }
    
    // TODO Insert setOnLeave() method here

    public void setOnLeave(boolean status) {
        this.onLeave = status;
    }
    
    // TODO Insert toString() method here

    public String toString() {
        return ("id:" + this.staffId + " " + this.name + " is teaching " + this.papers.length + " papers.");
    }
    
    // TODO Insert teachesMorePapersThan() method here

    public boolean teachesMorePapersThan(Lecturer lecturer) {
        if (this.papers.length > lecturer.papers.length) {
            return true;
        }
        else {
            return false;
        }
    }

}


