package ictgradschool.industry.lab04.ex06;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    public String brand;
    public String model;
    public double price;
    
    public MobilePhone(String brand, String model, double price) {
        this.brand = brand;
        this.model= model;
        this.price = price;
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    // TODO Insert getModel() method here

    public String getModel() {
        return this.model;
    }
    
    // TODO Insert setModel() method here

    public void setModel(String newModel) {
        this.model = newModel;
    }

    // TODO Insert getPrice() method here

    public double getPrice() {
        return this.price;
    }
    
    // TODO Insert setPrice() method here

    public void setPrice(double newPrice) {
        this.price = newPrice;
    }
    
    // TODO Insert toString() method here

    public String toString() {
        return (this.brand + " " + this.model + " which cost $" + this.price);
    }
    
    // TODO Insert isCheaperThan() method here

    public boolean isCheaperThan(MobilePhone mobile) {
        if (this.price < mobile.price) {
            return true;
        }
        else {
            return false;
        }
    }
    
    // TODO Insert equals() method here

    public boolean equals(MobilePhone mobile) {
        if (this.model.equals(mobile.model)) {
            return true;
        }
        else {
            return false;
        }
    }

}


