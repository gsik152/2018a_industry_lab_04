package ictgradschool.industry.lab04.ex05;

public class Pattern {
    public int multiplier;
    public char character;

    public Pattern(int multiplier, char character) {
        this.multiplier = multiplier;
        this.character = character;
    }

    public int getNumberOfCharacters() {
        return multiplier;
    }

    public void setNumberOfCharacters(int newMulti) {
        this.multiplier = newMulti;
    }

    public String toString() {
        String str = "";
        for (int i = 0; i < multiplier; i++) {
            str += character;
        }
        return str;
    }

}
