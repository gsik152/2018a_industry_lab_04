package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

import java.security.Key;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    public static final int PAPER = 2;
    public static final int SCISSORS = 3;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.

    public void start() {


        System.out.print("Hi! What is your name? ");
        String name = Keyboard.readInput();
        int choice = 0;
        while (choice != 4) {
            System.out.println();
            System.out.println("1. Rock");
            System.out.println("2. Paper");
            System.out.println("3. Scissors");
            System.out.println("4. Quit");
            System.out.print("Enter choice: ");
            choice = Integer.parseInt(Keyboard.readInput());
            if (choice == 4) {
                break;
            }
            System.out.println();
            displayPlayerChoice(name, choice);
            int computerChoice = (int) Math.ceil(Math.random() * 3);
            displayPlayerChoice("Computer", computerChoice);
            if (choice == computerChoice) {
                System.out.println("No one wins because" + getResultString(choice, computerChoice));
            } else if (userWins(choice, computerChoice)) {
                System.out.println(name + " wins because " + getResultString(choice, computerChoice) + ".");
            } else {
                System.out.println("Computer wins because " + getResultString(choice, computerChoice) + ".");
            }
        }

        System.out.println("Goodbye " + name + ". Thanks for playing :)");

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
    }


    public void displayPlayerChoice(String name, int choice) {
        if (choice == ROCK) {
            System.out.println(name + " chose rock.");
        }
        else if (choice == PAPER) {
            System.out.println(name + " chose paper.");
        }
        else if (choice == SCISSORS) {
            System.out.println(name + " chose scissors.");
        }
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        if (playerChoice == ROCK && computerChoice == SCISSORS) {
            return true;
        }
        else if (playerChoice == PAPER && computerChoice == ROCK) {
            return true;
        }
        else if (playerChoice == SCISSORS && computerChoice == PAPER) {
            return true;
        }
        else {
            return false;
        }
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        if (playerChoice == computerChoice) {
            return TIE;
        }
        else if ((playerChoice == PAPER && computerChoice == ROCK) || (playerChoice == ROCK && computerChoice == PAPER)) {
            return PAPER_WINS;
        }
        else if ((playerChoice == ROCK && computerChoice == SCISSORS) || (playerChoice == SCISSORS && computerChoice == ROCK)) {
            return ROCK_WINS;
        }
        else {
            return SCISSORS_WINS;
        }
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
